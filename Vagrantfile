require 'yaml'

CurrentDir = File.dirname(File.expand_path(__FILE__))
VagrantVariablesFileContent = YAML.load_file("#{CurrentDir}/VagrantVariables.yml")
VagrantVariables = VagrantVariablesFileContent['k8s-local-test']

Vagrant.configure("2") do |config|
    config.ssh.insert_key = false

    config.vm.provider VagrantVariables['vm_provider'] do |v|
        v.memory = 2048
        v.cpus = 4
    end
      
    config.vm.define "k8s-master" do |master|
        master.vm.box = VagrantVariables['image_name']
        master.vm.network "private_network", ip: VagrantVariables['subnet'] + "254"
        master.vm.synced_folder './shared_folder', '/home/vagrant/shared'
        master.vm.hostname = "k8s-master"
        master.vm.provision "ansible" do |ansible|
            ansible.playbook = "playbooks/master-playbook.yml"
            ansible.extra_vars = {
                node_ip: VagrantVariables['subnet'] + "254",
                kube_version: VagrantVariables['kube_pkg_version'],
            }
        end
    end

    (1..VagrantVariables['number_of_nodes']).each do |i|
        config.vm.define "node-#{i}" do |node|
            node.vm.box = VagrantVariables['image_name']
            node.vm.network "private_network", ip: VagrantVariables['subnet'] + "#{i + 1}"
            node.vm.hostname = "node-#{i}"
            node.vm.provision "ansible" do |ansible|
                ansible.playbook = "playbooks/node-playbook.yml"
                ansible.extra_vars = {
                    node_ip: VagrantVariables['subnet'] + "#{i + 1}",
                    kube_version: VagrantVariables['kube_pkg_version'],
                }
            end
        end
    end
end
